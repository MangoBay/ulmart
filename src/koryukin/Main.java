package koryukin;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
    private static final String PATH = "./src/koryukin/resources/";
    private static final String FILE_INPUT = PATH + "input.txt";
    private static final String FILE_OUTPUT = PATH + "output.txt";

    public static void main(String[] args) {
        fileWriter(encryption(fillArray(fileReader())));
    }

    /**
     * The first 16 bits store the age of the person, the second 16 bits - the number of entries
     * Array index is age, index value - number of occurrences.
     * It looks like this:
     * 0000_0000_0000_0010___0000_0000_0001_0101 == 21(2)            131093
     * 0000_0000_0000_0011___0000_0000_0001_0001 == 17(3)            196625
     * 0000_0000_0000_0001___0000_0000_0000_0000 == 0(1)             65536
     *
     * @param count array of ages
     * @return encryption array
     */
    private static int[] encryption(int[] count) {
        int length = count.length;
        int[] resultArray = new int[length];
        for (int age = 0; age < length; age++) {
            if (count[age] != 0) {
                int shift = count[age] << 16;
                resultArray[age] = shift + age;
            }
        }
        return resultArray;
    }

    /**
     * Converts data from one row to an array in which age is an index and the number of occurrences is an index value
     *
     * @param str data
     * @return array
     */
    private static int[] fillArray(String str) {
        int[] counterArray = new int[200];
        String[] s = str.split(" ");
        for (String s1 : s) {
            int number = Integer.parseInt(s1);
            counterArray[number] += 1;
        }
        return counterArray;
    }

    private static void fileWriter(int[] array) {
        try (FileWriter fileWriter = new FileWriter(FILE_OUTPUT)) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int value : array) {
                if (value != 0) {
                    stringBuilder.append(value).append(" ");
                }
            }
            fileWriter.write(stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Concatenates all data on one line. ( It was possible to make the union immediately into an array )
     *
     * @return string containing all the data
     */
    private static String fileReader() {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(FILE_INPUT))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                stringBuilder.append(sCurrentLine.toLowerCase());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
